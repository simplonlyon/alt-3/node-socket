
import {Server} from 'socket.io';
import { Message } from './entities';

const list:string[] = [];
/**
 * ça c'est juste pour rigoler, plutôt que manipuler la liste d'au dessus
 * on crée un Proxy de cette liste et on dit que dès que la taille de la liste
 * change on emit la liste des users à tous les clients connectés
 */
const usernameList = new Proxy(list, {
    set(target, key:any, value:any) {
        target[key] = value;
        if(key == 'length') {
            io.sockets.emit('user-list', usernameList);
        }
        return true;
    }
});

const messageList:Message[] = []

const io = new Server({
    cors:{
        origin: '*'
    }
});



io.on('connection', (socket) => {
    let username:string;
    
    console.log('client connected '+socket.id);
    socket.emit('user-list', usernameList);

    socket.on('disconnect', () => {
        usernameList.splice(usernameList.indexOf(username), 1);
        console.log(usernameList);
    })

    socket.emit('greeting', 'Hello your id is ' +socket.id);
    /**
     * Quand un client envoie un event pour choisir son username, on ajoute celui
     * ci à la liste si il n'est pas déjà dans la liste
     */
    socket.on('choose-username', (data:string, callback:Function) => {
        if(usernameList.includes(data)) {
            //Ce callback vient du front et s'exécute côté front, c'est assez épatant
            callback({
                status: 'error',
                msg: 'Username already taken'
            });
            return;
        }
        username = data;
        usernameList.push(data);
        socket.emit('message-list', messageList);
        callback({
            status: 'ok'
        })
    });
    /**
     * Quand on post un message, on l'ajoute au début de la liste de message et on
     * envoie le nouveau message à tout le monde
     */
    socket.on('message-post', (content:string) => {
        if(!content) {
            return;
        }
        const message:Message = {
            postDate: new Date(),
            username,
            content
        };
        messageList.unshift(message);
        io.sockets.emit('new-message', message);
    });
    /**
     * Quand quelqu'un commence à taper, on envoie un event à tous les autres pour
     * leur dire
     */
    socket.on('typing', () => {
        socket.broadcast.emit('typing', username);
    });
    socket.on('typing-stop', () => {
        socket.broadcast.emit('typing-stop', username);

    });
});


io.listen(8000);